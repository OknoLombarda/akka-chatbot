package me.chatbot;

import akka.actor.typed.Terminated;
import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

public class ChatBotMain extends AbstractBehavior<ChatBotMain.Start> {
    public static final class Start {
        public final String question;

        public Start(String question) {
            this.question = question;
        }
    }

    private final ActorRef<RequestHandler.Response> chatBot;

    public static Behavior<Start> create() {
        return Behaviors.setup(ChatBotMain::new);
    }

    public ChatBotMain(ActorContext<Start> context) {
        super(context);

        chatBot = context.spawn(ChatBot.create(), "chatBot");
        context.watch(chatBot);
    }

    @Override
    public Receive<Start> createReceive() {
        return newReceiveBuilder()
                .onMessage(Start.class, this::onStart).onSignal(Terminated.class, this::onTerminated).build();
    }

    public Behavior<Start> onTerminated(Terminated signal) {
        return Behaviors.stopped();
    }

    private Behavior<Start> onStart(Start command) {
        ActorRef<RequestHandler.Request> requestHandler = getContext().spawn(RequestHandler.create(), "requestHandler");

        if (command.question != null && !command.question.isEmpty()) {
            requestHandler.tell(new RequestHandler.Request(command.question, chatBot));
        } else {
            chatBot.tell(new RequestHandler.Response(null, requestHandler));
        }

        return this;
    }
}
