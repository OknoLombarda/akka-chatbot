package me.chatbot;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.util.Scanner;

public class ChatBot extends AbstractBehavior<RequestHandler.Response> {
    private Scanner in;

    private ChatBot(ActorContext<RequestHandler.Response> context) {
        super(context);
    }

    public static Behavior<RequestHandler.Response> create() {
        return Behaviors.setup(ChatBot::new);
    }

    @Override
    public Receive<RequestHandler.Response> createReceive() {
        return newReceiveBuilder().onMessage(RequestHandler.Response.class, this::onResponse).build();
    }

    private Behavior<RequestHandler.Response> onResponse(RequestHandler.Response response) {
        if (in == null) {
            in = new Scanner(System.in);
        }

        String input;

        if (response.answer != null) {
            System.out.println(response.answer);

            boolean done = false;
            do {
                System.out.println("\nAsk next question? [Y/n]");
                input = in.nextLine();
                if (input.equalsIgnoreCase("y") || input.isEmpty()) {
                    done = true;
                } else if (input.equalsIgnoreCase("n")) {
                    return Behaviors.stopped();
                } else {
                    System.out.println("Wrong input");
                }
            } while (!done);
        }

        do {
            System.out.println("Please, ask your question");
            input = in.nextLine();
        } while (input.trim().isEmpty());

        response.from.tell(new RequestHandler.Request(input, getContext().getSelf()));

        return this;
    }
}
