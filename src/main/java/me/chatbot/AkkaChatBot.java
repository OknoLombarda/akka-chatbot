package me.chatbot;

import akka.actor.typed.ActorSystem;

public class AkkaChatBot {
    public static void main(String[] args) {
        final ActorSystem<ChatBotMain.Start> chatBotMain = ActorSystem
                                                            .create(ChatBotMain.create(), "akkaChatBot");
        chatBotMain.tell(new ChatBotMain.Start(args.length > 0 ? args[0] : ""));
    }
}
