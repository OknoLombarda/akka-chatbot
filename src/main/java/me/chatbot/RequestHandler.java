package me.chatbot;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RequestHandler extends AbstractBehavior<RequestHandler.Request> {
    private static final String DATA = "{ \"context\": [ \"%s\" ] }";

    public static final class Response {
        public final String answer;
        public final ActorRef<Request> from;

        public Response(String answer, ActorRef<Request> from) {
            this.answer = answer;
            this.from = from;
        }

        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null || getClass() != obj.getClass()) return false;

            Response other = (Response) obj;
            return Objects.equals(answer, other.answer) && Objects.equals(from, other.from);
        }
    }

    public static final class Request {
        public final String question;
        public final ActorRef<Response> replyTo;

        public Request(String question, ActorRef<Response> replyTo) {
            this.question = question;
            this.replyTo = replyTo;
        }
    }

    private RequestHandler(ActorContext<Request> context) {
        super(context);
    }

    public static Behavior<Request> create() {
        return Behaviors.setup(RequestHandler::new);
    }

    @Override
    public Receive<Request> createReceive() {
        return newReceiveBuilder().onMessage(Request.class, this::onRequest).build();
    }

    private Behavior<Request> onRequest(Request command) {
        byte[] out = String.format(DATA, command.question).getBytes(StandardCharsets.UTF_8);
        String answer = "";

        try {
            URL url = new URL("https://odqa.demos.ivoice.online/model");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.getOutputStream().write(out);

            if (connection.getResponseCode() == 200) {
                StringBuilder sb = new StringBuilder();
                Scanner sc = new Scanner(connection.getInputStream());

                while (sc.hasNextLine()) {
                    sb.append(sc.nextLine());
                }

                Matcher matcher = Pattern.compile("\\[\".+\"\\]").matcher(sb.toString());
                if (matcher.find()) {
                    answer = matcher.group();
                    answer = answer.substring(2, answer.length() - 2);
                }
            } else {
                answer = "Error. Response code: ".concat(String.valueOf(connection.getResponseCode()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        command.replyTo.tell(new Response(answer, getContext().getSelf()));

        return this;
    }
}
