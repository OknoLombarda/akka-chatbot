package me.chatbot;

import akka.actor.testkit.typed.javadsl.TestKitJunitResource;
import akka.actor.testkit.typed.javadsl.TestProbe;
import akka.actor.typed.ActorRef;
import org.junit.ClassRule;
import org.junit.Test;

public class AkkaChatBotTest {
    @ClassRule
    public static final TestKitJunitResource testKit = new TestKitJunitResource();

    public static final String msg = "A program taught in Russian will cost you 250,000 rubles per year. " +
            "The fees on English-taught programs are higher, at 400,000 rubles per year.";

    @Test
    public void testGreeterActorSendingOfGreeting() {
        TestProbe<RequestHandler.Response> testProbe = testKit.createTestProbe();
        ActorRef<RequestHandler.Request> underTest = testKit.spawn(RequestHandler.create(), "requestHandler");
        underTest.tell(new RequestHandler.Request("What are the tuition fees?", testProbe.getRef()));
        testProbe.expectMessage(new RequestHandler.Response(msg, underTest));
    }
}
